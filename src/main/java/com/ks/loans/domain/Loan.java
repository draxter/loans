package com.ks.loans.domain;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Loan {
    @Id
    @GeneratedValue
    Long id;
    BigInteger amount;
    Integer termDays;
    LocalDate dueDate;

    public Loan() {
    }

    public Loan(Long id, BigInteger amount, Integer termDays, LocalDate dueDate) {
        this.id = id;
        this.amount = amount;
        this.termDays = termDays;
        this.dueDate = dueDate;
    }

    public Loan(BigInteger amount, Integer termDays, LocalDate baseTime) {
        this.amount = amount;
        this.termDays = termDays;
        this.dueDate = baseTime.plusDays(termDays);
    }

    public Long getId() {
        return id;
    }

    public BigInteger getAmount() {
        return amount;
    }

    public Integer getTermDays() {
        return termDays;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Loan loan = (Loan) o;
        return Objects.equals(id, loan.id) && Objects.equals(amount, loan.amount) && Objects.equals(termDays, loan.termDays) && Objects.equals(dueDate, loan.dueDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, amount, termDays, dueDate);
    }
}

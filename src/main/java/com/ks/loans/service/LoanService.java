package com.ks.loans.service;

import com.ks.loans.domain.Loan;
import java.math.BigInteger;

public interface LoanService {
    Loan read(Long id);

    Loan save(BigInteger amount, Integer termDays);

    Loan extend(Long id);
}

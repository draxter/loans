package com.ks.loans.controller;

import com.ks.loans.domain.Loan;
import com.ks.loans.service.LoanService;
import java.math.BigInteger;
import java.time.LocalDate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LoanControllerTest {

    @InjectMocks
    private LoanController loanController;

    @Mock
    private LoanService loanService;

    @Test
    void readLoan() {
        Loan expectedLoan = new Loan(1L, BigInteger.valueOf(20L), 10, LocalDate.now());
        when(loanService.read(eq(1L))).thenReturn(expectedLoan);

        ResponseEntity<Loan> result = loanController.read(1L);

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(expectedLoan, result.getBody());
    }

    @Test
    void saveLoan() {
        final BigInteger amount = BigInteger.valueOf(20L);
        final Integer termDays = 10;

        Loan expectedLoan = new Loan(1L, amount, termDays, LocalDate.now().plusDays(termDays));
        when(loanService.save(eq(amount), eq(termDays))).thenReturn(expectedLoan);

        ResponseEntity<Loan> result = loanController.save(amount, termDays);

        assertEquals(HttpStatus.CREATED, result.getStatusCode());
        assertEquals(expectedLoan, result.getBody());
    }

    @Test
    void extendLoan() {
        final Long id = 1L;
        final BigInteger amount = BigInteger.valueOf(20L);
        final int termDays = 10;

        Loan expectedLoan = new Loan(id, amount, termDays, LocalDate.now().plusDays(termDays));
        when(loanService.extend(eq(id))).thenReturn(expectedLoan);

        ResponseEntity<Loan> result = loanController.extend(id);

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(expectedLoan, result.getBody());
    }
}
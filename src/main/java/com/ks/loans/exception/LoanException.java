package com.ks.loans.exception;

public class LoanException extends RuntimeException {
    public LoanException(String message) {
        super(message);
    }
}

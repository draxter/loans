package com.ks.loans.repository;

import com.ks.loans.domain.Loan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LoanJpaRepository extends CrudRepository<Loan, Long> {
}

package com.ks.loans.service;

import com.ks.loans.configuration.LoanConfiguration;
import com.ks.loans.domain.Loan;
import com.ks.loans.exception.LoanException;
import com.ks.loans.exception.LoanNotFoundException;
import com.ks.loans.repository.LoanJpaRepository;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LoanServiceImpl implements LoanService {

    @Autowired
    private LoanConfiguration loanConfiguration;

    @Autowired
    private LoanJpaRepository loanRepository;

    @Override
    public Loan read(Long id) {

        Optional<Loan> maybeLoan = loanRepository.findById(id);
        return maybeLoan.orElseThrow(() -> new LoanNotFoundException(String.format("Loan with id %d is not found", id)));
    }

    @Override
    public Loan save(BigInteger amount, Integer termDays) {
        LocalDateTime now = LocalDateTime.now();
        validate(amount, now.toLocalTime());
        Loan loan = new Loan(amount, termDays, now.toLocalDate());

        return loanRepository.save(loan);
    }

    @Override
    public Loan extend(Long id) {
        Loan loan = read(id);
        Loan extendedLoan = new Loan(loan.getId(), loan.getAmount(),
                loan.getTermDays() + loanConfiguration.getExtendTimeInDays(),
                loan.getDueDate().plusDays(loanConfiguration.getExtendTimeInDays()));

        return loanRepository.save(extendedLoan);
    }

    private void validate(BigInteger amount, LocalTime now) {
        if (!(loanConfiguration.getMinAmount().compareTo(amount) <= 0 && loanConfiguration.getMaxAmount().compareTo(amount) >= 0)) {
            throw new LoanException("Loan amount is not in allowed range");
        }
        if (loanConfiguration.getMaxAmount().compareTo(amount) == 0
                && loanConfiguration.getRestrictedStartTime().isBefore(now)
                && loanConfiguration.getRestrictedEndTime().isAfter(now)) {
            throw new LoanException(String.format("Loan for max amount is prohibited in restricted time: %s and %s",
                    loanConfiguration.getRestrictedStartTime(), loanConfiguration.getRestrictedEndTime()));
        }
    }
}

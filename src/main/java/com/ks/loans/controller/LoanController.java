package com.ks.loans.controller;

import com.ks.loans.domain.Loan;
import com.ks.loans.exception.LoanException;
import com.ks.loans.exception.LoanNotFoundException;
import com.ks.loans.service.LoanService;
import java.math.BigInteger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/loan")
public class LoanController {

    @Autowired
    private LoanService loanService;

    @GetMapping("/{id}")
    public ResponseEntity<Loan> read(@PathVariable Long id) {
        Loan loan = loanService.read(id);
        return ResponseEntity.ok(loan);
    }

    @PostMapping
    public ResponseEntity<Loan> save(@RequestParam BigInteger amount, @RequestParam Integer termDays) {
        Loan savedLoan = loanService.save(amount, termDays);
        return new ResponseEntity<Loan>(savedLoan, HttpStatus.CREATED);
    }

    @PutMapping("/{id}/extend")
    public ResponseEntity<Loan> extend(@PathVariable Long id) {

        Loan extendedLoan = loanService.extend(id);
        return ResponseEntity.ok(extendedLoan);
    }

    @ExceptionHandler(LoanException.class)
    public ResponseEntity<String> handleLoanException(LoanException exception) {
        return ResponseEntity.badRequest().body(exception.getMessage());
    }

    @ExceptionHandler(LoanNotFoundException.class)
    public ResponseEntity<String> handleLoanNotFoundException(LoanNotFoundException exception) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exception.getMessage());
    }
}

package com.ks.loans.configuration;

import java.math.BigInteger;
import java.time.LocalTime;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;


@ConfigurationProperties(prefix = "config.loan")
@ConstructorBinding
public class LoanConfiguration {

    private final BigInteger minAmount;
    private final BigInteger maxAmount;
    private final Integer minTermInDays;
    private final Integer maxTermInDays;
    private final Integer extendTimeInDays;
    private final LocalTime restrictedStartTime;
    private final LocalTime restrictedEndTime;

    public LoanConfiguration(BigInteger minAmount, BigInteger maxAmount, Integer minTermInDays, Integer maxTermInDays, Integer extendTimeInDays, LocalTime restrictedStartTime, LocalTime restrictedEndTime) {
        this.minAmount = minAmount;
        this.maxAmount = maxAmount;
        this.minTermInDays = minTermInDays;
        this.maxTermInDays = maxTermInDays;
        this.extendTimeInDays = extendTimeInDays;
        this.restrictedStartTime = restrictedStartTime;
        this.restrictedEndTime = restrictedEndTime;
    }

    public BigInteger getMinAmount() {
        return minAmount;
    }

    public BigInteger getMaxAmount() {
        return maxAmount;
    }

    public Integer getMinTermInDays() {
        return minTermInDays;
    }

    public Integer getMaxTermInDays() {
        return maxTermInDays;
    }

    public Integer getExtendTimeInDays() {
        return extendTimeInDays;
    }

    public LocalTime getRestrictedStartTime() {
        return restrictedStartTime;
    }

    public LocalTime getRestrictedEndTime() {
        return restrictedEndTime;
    }
}

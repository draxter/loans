package com.ks.loans.service;

import com.ks.loans.configuration.LoanConfiguration;
import com.ks.loans.domain.Loan;
import com.ks.loans.exception.LoanException;
import com.ks.loans.exception.LoanNotFoundException;
import com.ks.loans.repository.LoanJpaRepository;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LoanServiceImplTest {

    @Mock
    LoanConfiguration loanConfiguration;
    @InjectMocks
    private LoanServiceImpl loanService;
    @Mock
    private LoanJpaRepository loanRepository;

    @Test
    void read() {
        Loan expectedLoan = new Loan(1L, BigInteger.valueOf(20L), 10, LocalDate.now());
        when(loanRepository.findById(eq(1L))).thenReturn(Optional.of(expectedLoan));

        Loan result = loanService.read(1L);

        assertEquals(expectedLoan, result);
    }

    @Test
    void readUnknownLoanThrowsException() {
        when(loanRepository.findById(eq(1L))).thenReturn(Optional.empty());

        assertThrows(LoanNotFoundException.class, () -> loanService.read(1L));
    }


    @Test
    void save() {
        final BigInteger amount = BigInteger.valueOf(20L);
        final int termDays = 10;
        Loan expectedLoan = new Loan(null, amount, termDays, LocalDate.now().plusDays(termDays));
        ArgumentCaptor<Loan> loanArgumentCaptor = ArgumentCaptor.forClass(Loan.class);
        when(loanRepository.save(loanArgumentCaptor.capture())).thenReturn(expectedLoan);
        when(loanConfiguration.getMinAmount()).thenReturn(BigInteger.valueOf(10));
        when(loanConfiguration.getMaxAmount()).thenReturn(BigInteger.valueOf(100));

        Loan result = loanService.save(amount, termDays);

        assertEquals(expectedLoan, result);
        assertEquals(expectedLoan, loanArgumentCaptor.getValue());
    }

    @Test
    void saveWithAmountBelowThresholdThrowsException() {
        when(loanConfiguration.getMinAmount()).thenReturn(BigInteger.valueOf(10));

        assertThrows(LoanException.class, () -> loanService.save(BigInteger.valueOf(5), 10));
    }

    @Test
    void saveWithAmountAboveThresholdThrowsException() {
        when(loanConfiguration.getMinAmount()).thenReturn(BigInteger.valueOf(10));
        when(loanConfiguration.getMaxAmount()).thenReturn(BigInteger.valueOf(30));

        assertThrows(LoanException.class, () -> loanService.save(BigInteger.valueOf(45), 10));
    }

    @Test
    void saveInRestrictedWithMaxAmountThrowsException() {
        when(loanConfiguration.getMinAmount()).thenReturn(BigInteger.valueOf(10));
        when(loanConfiguration.getMaxAmount()).thenReturn(BigInteger.valueOf(30));
        when(loanConfiguration.getRestrictedStartTime()).thenReturn(LocalTime.now().minusMinutes(5));
        when(loanConfiguration.getRestrictedEndTime()).thenReturn(LocalTime.now().plusMinutes(5));

        assertThrows(LoanException.class, () -> loanService.save(BigInteger.valueOf(30), 10));
    }

    @Test
    void extend() {
        //TODO
    }
}